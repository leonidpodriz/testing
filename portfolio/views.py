from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy

from portfolio.models import Post, Portfolio, Skill, Meta
from portfolio.forms import ContactFormHTML, UserCreationFormWithoutHelpText
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login
from django.contrib.auth import login, authenticate
from django.http import HttpResponse
from django.views.generic import DetailView, FormView



def homepage(request):
    portfolio_objects = Portfolio.objects.all()
    posts = Post.objects.all()
    contact_form = ContactFormHTML()
    skills = Skill.objects.all()

    if request.method == "POST":
        contact_form = ContactFormHTML(request.POST)

        if contact_form.is_valid():
            contact_form.save()

    context = {'name': 'Kate Stepannikova',
               'job_list': ["Python", "That`s all"],
               'posts': posts,
               'portfolio': portfolio_objects,
               'contact_form': contact_form,
               'skills': skills,
               "contacts":
                   {"phone": "+380678482590",
                    'email': 'kate.stepannikova@gmail.com',
                    'address': 'Kyiv, BVS 8/20, 02101'},

               }

    return render(request, 'portfolio/index.html', context)


def print_context(request):
    context = {

    }
    return render(request, 'portfolio/context.html', context)


# def single_blog(request, pk):
#     # post = Post.objects.get(pk=pk)
#     post = get_object_or_404(Post, pk=pk)
#     context = {
#         'post': post
#     }
#     return render(request, 'portfolio/blog-single.html', context)

class BlogSingle(DetailView):
    template_name = 'portfolio/blog-single.html'
    model = Post


# def login_page(request):
#     form = AuthenticationForm()
#     if request.method == "POST":
#         form = AuthenticationForm(data=request.POST)
#         if form.is_valid():
#             user = form.get_user()
#             login(request, user=user)
#
#     context = {
#         'form': form,
#     }
#     return render(request, 'portfolio/login.html', context)

class Login(FormView):
    form_class = AuthenticationForm
    template_name = 'portfolio/login.html'
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super().form_valid(form)


# def register_page(request):
#     form = UserCreationFormWithoutHelpText()
#     if request.method == "POST":
#         form = UserCreationForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('login')
#     context = {
#         'form': form,
#     }
#     return render(request, 'portfolio/register.html', context)

class Register(FormView):
    form_class = UserCreationForm
    template_name = 'portfolio/register.html'
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)